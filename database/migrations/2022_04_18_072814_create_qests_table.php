<?php

use App\Models\Qqq;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bio_id')->constrained()->onDelete('cascade');
            foreach(Qqq::all() as $q){
                $table->string('q' . $q->id)->nullable();
            }
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qests');
    }
}
