<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [App\Http\Controllers\Form::class, 'show']);
// Route::post('/send', [App\Http\Controllers\Form::class, 'add'])->name('send');

Route::get('/', [App\Http\Controllers\Main::class, 'home'])->name('home');
Route::post('/bio', [App\Http\Controllers\Main::class, 'bio'])->name('bio');
Route::post('/questions', [App\Http\Controllers\Main::class, 'questions'])->name('questions');


Route::get('/list', [App\Http\Controllers\Main::class, 'list'])->name('list');

Route::get('/addQ', [App\Http\Controllers\Main::class, 'addQForm'])->name('addQForm');
Route::post('/addQ', [App\Http\Controllers\Main::class, 'addQ'])->name('addQ');
