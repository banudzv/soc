<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>add-form</title>
</head>
<body>
  <a href="{{route('home')}}">home</a>
  <hr>
  <div>
    @if(session('success'))
    {{session('success')}}
    @endif
</div>
  <form action="{{route('addQ')}}" method="POST">
    @csrf
    <div>
      <label for="text">Запитання: </label>
      <br>  
      <input type="text" name="text" id="text">
    </div>
    <div>
      {{-- <label for=""> </label>
      <br>  
      <input type="text" name="type" id="type"> --}}

      <label for="type">Тип відповіді: </label>
      <br>
      <select name="type" id="type">
        <option value="text" selected>текст</option>
        <option value="number">число</option>
      </select>
    </div>
    <div>
      <label for="select">З варіантами відповідей</label>
      <br>
      <select name="select" id="select">
        <option value="1">так</option>
        <option value="0" selected>ні</option>
      </select>
    </div>
    <div>
      <label for="options">Варіанти для селект: </label>
      <br>  
      <input type="text" name="options" id="options">
    </div>
    <button type="submit">Відправити</button>
  </form>
  
</body>
</html>