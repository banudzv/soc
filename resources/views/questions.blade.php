<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>questions</title>
</head>
<body>
  <a href="{{route('home')}}">home</a>
    <hr>

<form action="{{route('questions')}}" method="POST">
    @csrf
    <input type="hidden" name="bio_id" value="{{$str}}">
<div> 
  @foreach($q as $qq)
    <div>
      <p><span>{{$qq->id}}. </span>{{$qq->text}}</p>
      @if($qq->select)
      <?php
        $options = explode(', ', $qq->options)
      ?>
      <select name="q{{$qq->id}}">
          @foreach($options as $o)
          <option value="{{$o}}">{{$o}}</option>
          @endforeach
      </select>
      @else
      <input type="{{$qq->type}}" name="q{{$qq->id}}">
      @endif
      <hr>
    </div>
  @endforeach
</div>
<button type="submit">send</button>
</form>

</body>
</html>
