<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>list</title>
</head>
<body>
  <a href="{{route('home')}}">home</a>
  <hr>
  <div style="display:flex; flex-wrap: wrap">
        @foreach($bios as $bio)
         <div style="background: lightgray; padding: 10px; margin: 10px; width: 47%">
            <div style="background: rgb(240, 189, 189); border: 2px solid black; padding: 10px; margin: 10px; font-size: 20px">
              <p style="margin: 3px">Імя: {{$bio->name}}</p>
              <p style="margin: 3px">Вік: {{$bio->age}}</p>
              <p style="margin: 3px">Місто: {{$bio->city}}</p>
              <p style="margin: 3px">Стать: {{$bio->sex}}</p>
            </div>
            <div style="background: rgb(205, 238, 195); border: 2px solid black; padding: 10px;">
              <b> Відповіді на запитання:</b> <br>
              @foreach($bio->questions->attributesToArray() as $key => $value)
                  @if(str_contains($key, 'q'))
                      <p style="margin: 3px">{{substr($key, 1)}}. 
                        @foreach($qqq as $q)
                          @if($q['id'] == substr($key, 1))
                          {{$q['text']}}
                          @endif
                        @endforeach
                      </p>
                      <p style="margin: 3px">{{$value}}</p>
                      <hr>
                  @endif
              @endforeach
            </div>
          </div>
        @endforeach
  </div>
</body>
</html>
