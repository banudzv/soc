<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Qqq extends Model
{
    use HasFactory;

    protected $fillable = ['text', 'type', 'select', 'options'];

}
