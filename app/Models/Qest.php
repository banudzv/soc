<?php

namespace App\Models;

use App\Models\Bio;
use App\Models\Qqq;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Qest extends Model
{
    use HasFactory;

    protected function arr(){
        $arr = [];
        foreach(Qqq::all() as $q){
            array_push($arr, 'q' . $q->id);
        }
        array_unshift($arr, 'bio_id');
        return $arr;
    }


    protected $fillable = ['bio_id', 'q1', 'q2', 'q6', 'q7', 'q8', 'q10'];
//    public $rrr = ['fff', 'rrr'];
    // protected $fillable = $this->arr();

    public function bio(){
        return $this->belongsTo(Bio::class);
    }

}
