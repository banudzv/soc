<?php

namespace App\Models;

use App\Models\Bio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Question extends Model
{
    use HasFactory;
    protected $fillable = ['bio_id', 'q1'];

    public function bio(){
        return $this->belongsTo(Bio::class);
    }

}
