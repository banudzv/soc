<?php

namespace App\Models;

use App\Models\Qest;
use App\Models\Question;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bio extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'age', 'city', 'sex'];

    public function questions(){
        return $this->hasOne(Qest::class);
    }

}
