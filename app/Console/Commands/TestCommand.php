<?php

namespace App\Console\Commands;

use App\Models\Test2;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test2:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Просто прикалуюсь';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        sleep(10);
        Test2::create([
            'event' => 'шось'
        ]);

        return 0;
    }
}
