<?php

namespace App\Http\Controllers;

use App\Models\Bio;
use App\Models\Qqq;
use App\Models\Qest;
use App\Models\Question;
use Illuminate\Http\Request;

class Main extends Controller
{
    public function toInput($array){
        $html = [];
        foreach($array as $q){
            array_push($html, $q['text']);
        };
       return $html;
   }

    public function home(){
        // $q = Qest::arr();
        // dd($q);
        return view('home');
    }
    public function bio(Request $request)
    {
        $bio = Bio::create($request->all());
        if($bio){
           $str = $bio->id;
           $q = Qqq::all();
        return view('questions', compact(['str', 'q']));
        }
        return redirect()->back();

    }
    public function questions(Request $request)
    {
        $q = Qest::create($request->all());
        $message = 'Все yt нормально';
        if($q){
           $message = 'Все нормально';
        return redirect('/')->with('success', $message);
        }
        return redirect()->back();
    }

    public function list()
    {
        $bios = Bio::all();
        $qqq = Qqq::all()->toArray();
        return view('list', compact('bios', 'qqq'));
    }

    public function addQForm(){

        return view('add-form');
    }

    public function addQ(Request $request){
        $qqq = Qqq::create($request->all());
        $message = 'Виникли проблеми';
        if($qqq){
            $message = 'Питання додано';
        }
        return redirect()->back()->with('success', $message);
    }
}
