<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Events\SendEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class Form extends Controller
{
    public function show(){
        return view('form');
    }

    public function add(Request $request){
        $r = Test::create($request->all());
        Log::info('Info messege from Form, name = '. $request->name);
//        event(new SendEvent($r));
        Artisan::queue('test2:name');
        return redirect()->back();
    }
}
