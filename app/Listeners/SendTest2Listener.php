<?php

namespace App\Listeners;

use App\Models\Test2;
use App\Events\SendEvent;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTest2Listener
{
    // /**
    //  * Create the event listener.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     //
    // }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SendEvent  $event
     * @return void
     */
    public function handle(SendEvent $event)
    {
        $e = $event->r->name . ' + ' . 'string';
        Test2::create([
            'event' => $e
        ]);
        Log::info('Info messege from SendTest2Listener, new str = '. $e);

    }
}
